const circlesComponent = (function(vNuvem) {
    const apiUrl = 'http://localhost:3001';
    const circlesUrl = apiUrl + '/circles';

    function _init() {
        getCirclesData();
    }

    function getCirclesData() {
        vNuvem.ajax(circlesUrl, makeInterface);
    }

    function makeInterface(data) {
        if (! data) {
            return;
        }

        data = JSON.parse(data);

        const circleListElement = vNuvem('#list-circles').elements[0];
        const documentFragment = new DocumentFragment();
        const dataLength = data.length;

        for(let i = 0; i < dataLength; i++) {
            let liElement = vNuvem.createElement('li', documentFragment, {class: 'list-circles-item '+ data[i].name.toLowerCase()});
            vNuvem.createElement('img', liElement,
                {
                    class: 'circle-size-'+ data[i].size,
                    src: apiUrl + '/' + data[i].image
                }
            );
            vNuvem.createElement('span', liElement, {class: 'list-circles-legend', text: data[i].name}
            );
        }

        circleListElement.append(documentFragment);
    }

    return {
        init: _init
    }
})(vNuvem);