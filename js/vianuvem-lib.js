/**
 * Já que no proposto dizia para escolher entre usar Jquery ou Js puro para desenvolver o teste,
 * resolvi usar js puro e criar uma lib de manipulação do DOM e alguns utilitários para a Vianuvem a fim de
 * mostrar meu nível de conhecimento em javascript.
 *
 **/

const vNuvem = function (selector) {
    return new vNuvem.app(selector);
};

vNuvem.app = function (selector) {
    const that = this;

    that.elements = false;
    that.element = false;
    that.response = false;
    that._initialized = false;


    that.init = function (selector) {
        if (!selector) {
            return that;
        }

        if (typeof selector === 'object') {
            if (selector.length && selector.length > 1 && selector !== window) {
                that.elements = selector;
                return that;

            } else {
                that.element = selector;
            }

            that.elements = [selector];

        } else {
            that.elements = that.getFromDOM(selector);
        }

        return that;
    };

    that.getFromDOM = function (selector) {
        return document.querySelectorAll(selector);
    };

    that.iterateElements = function(callback) {
        const elementsLength = that.elements.length;

        if (elementsLength) {
            that.response = [];
        }

        for (let i = 0; i < elementsLength; i++) {
            that.element = that.elements[i];

            if (that.element) {
                that.response.push(callback());
            }
        }

        if (that.response.length == 1) {
            that.response = that.response[0];
        }
    };

    that.addClass = function (className) {
        that.iterateElements(function () {
            return that.element.classList.add(className);
        });

        return that;
    };

    that.removeClass = function (className) {
        that.iterateElements(function () {
            return that.element.classList.remove(className);
        });

        return that;
    };

    that.hasClass = function (className) {
        let functionReturn = false;

        that.iterateElements(function() {
            functionReturn = functionReturn || (that.element.className.indexOf(className) > -1);
        });

        return functionReturn;
    }

    that.toggleClass = function (className) {
        that.iterateElements(function() {
            if (document.body.classList) {
                return that.element.classList.toggle(className);
            }

            // fallback
            let classString = that.element.className,
                nameIndex   = classString.indexOf(className);

            if (nameIndex === -1) {
                classString += ' ' + className;

            } else {
                classString = classString.substr(0, nameIndex) + classString.substr(nameIndex+className.length);
            }

            that.element.className = classString;
        });

        return that;
    };

    that.html = function (content) {
        that.iterateElements(function() {
            return that.element.innerHTML = content;
        });

        return that;
    };

    that.text = function (content) {
        that.iterateElements(function() {
            if (typeof document.body.textContent === 'undefined') {
                return that.element.innerText = content;
            }

            return that.element.textContent = content;
        });

        return that;
    };

    that.remove = function () {
        that.iterateElements(function () {
            if (typeof document.body.remove === 'function') {
                return that.element.remove();
            }

            return that.element.parentElement.removeChild(that.element);
        });

        return that.response;
    };

    that.forEach = function forEach(fn, scope) {
        for(let i = 0, len = that.elements.length; i < len; i++) {
            fn.call(scope, that.elements[i], i, that.elements);
        }
    };

    that.data = function (attr, value) {
        if (!attr) {
            return false;
        }

        if (value) {
            that.iterateElements(function () {
                if (!document.body.dataset) {
                    that.element.setAttribute('data-' + attr, value);
                }

                that.element.dataset[attr] = value;
            });

        } else {
            that.iterateElements(function () {
                if (!document.body.dataset) {
                    return that.element.getAttribute('data-' + attr) || false;
                }

                return that.element.dataset[attr] || false;
            });
        }

        return that.response;
    };

    that.init(selector);

    return that;
};

vNuvem.external = {
    ajax: function (url, callback, data) {
        let x;

        try {
            x = new(window.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
            x.open(data ? 'POST' : 'GET', url, 1);
            x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            x.onreadystatechange = function () {
                x.readyState > 3 && callback && callback(x.responseText, x);
            };
            x.send(data);
        } catch (e) {
            window.console && console.log(e);
        }
    },

    createElement: function (elName, parentNode, properties) {
        parentNode = parentNode || document.body;

        const el = document.createElement(elName);
        const length = properties ? Object.keys(properties).length : 0;
        let nameKeys = '';

        if (length > 0 && typeof properties !== 'undefined') {
            nameKeys = Object.keys(properties);

            for (let i = 0; i < length; i += 1) {
                vNuvem.setAttribute(el, nameKeys[i], properties[nameKeys[i]]);
            }
        }

        parentNode.appendChild(el);
        return el;
    },

    setAttribute: function (el, attr, value) {
        if (attr === 'text') {
            if (document.body.textContent) {
                el.textContent = value;

            } else {
                el.innerText = value;
            }

            return;
        }

        el.setAttribute(attr, value);
    }
};

let method;

for (method in vNuvem.external) {
    vNuvem[method] = vNuvem.external[method];
}

vNuvem.external = undefined;