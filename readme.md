# Teste de Frontend para Vianuvem

Primeiramente você deve rodar `npm install` na pasta *root* para instalar todas as dependencias do projeto.

Então, para rodar o projeto rode `npm run start`.

`npm run start` vai iniciar um servidor web com apenas o front.

*Você precisa* rodar o servidor da api, que está na pasta *api*, basta entrar na pasta, dar um `npm install` e depois um `node server`;

*PRONTO!*  Tudo estará funcionando corretamente. Basta acessar *http://localhost:8080* e ver o projeto rodando.

## Criei uma lib javascript ao invés de usar o Jquery

Já que no proposto dizia para escolher entre usar Jquery ou Js puro para desenvolver o teste, resolvi usar js puro e criar uma lib de manipulação do DOM e alguns utilitários para a Vianuvem a fim de mostrar meu nível de conhecimento em javascript.